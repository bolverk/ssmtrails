# ssmtrails

## Purpose
This project generates a basemap for Garmin computers such as the Edge and
eTrex series which shows the off-road trails in the Sault Ste. Marie, ON area.
The goal is to provide routeable mapping for bicycles, skis, hiking,
snowmobiles, and ATVs.

## Build Requirements
1. Functioning make
2. Java Runtime Environment 1.8 or newer for mkgmap: https://jdk.java.net/11/ or https://java.com/
3. mkgmap from the OpenStreetmap project: http://www.mkgmap.org.uk/download/mkgmap.html
4. gpsbabel: https://www.gpsbabel.org/

## TODO
- Improve visual styles
- Add additional routes
- Create metadata files to go along side .gpx files
- Classify seasons that routes are passable
- Classify seasons that routes are permitted
- Classify types of vehicle that are permitted on routes
- Extend the build system to generate base maps that show only desired routes (such as routes permitted for bicycle in winter)
