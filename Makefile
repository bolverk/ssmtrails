JAVA ?= /usr/bin/java
MKGMAP ?= $(HOME)/mkgmap-r4485/mkgmap.jar
VERSION=1.0.4

ATV_SRC := $(wildcard ATV/*.gpx)
ATV_OBJ := $(patsubst ATV/%.gpx,ATV/%.osm,$(ATV_SRC))
Hike_SRC := $(wildcard Hike/*.gpx)
Hike_OBJ := $(patsubst Hike/%.gpx,Hike/%.osm,$(Hike_SRC))
Bike_SRC := $(wildcard Bike/*.gpx)
Bike_OBJ := $(patsubst Bike/%.gpx,Bike/%.osm,$(Bike_SRC))
Ski_SRC := $(wildcard Ski/*.gpx)
Ski_OBJ := $(patsubst Ski/%.gpx,Ski/%.osm,$(Ski_SRC))

all: gmapsupp.img

clean:
	rm -f $(ATV_OBJ) $(Hike_OBJ) $(Bike_OBJ) mystyle/xmytype.typ mystyle/mytype.typ || true
	rm -rf output/ || true

ATV/%.osm: ATV/%.gpx
	gpsbabel -i gpx -f $< -o osm,tag="highway:track;atv:designated" -F $@

Hike/%.osm: Hike/%.gpx
	gpsbabel -i gpx -f $< -o osm,tag="highway:path;route:hiking" -F $@

Bike/%.osm: Bike/%.gpx
	gpsbabel -i gpx -f $< -o osm,tag="highway:path;bicycle:designated;foot:yes" -F $@

Ski/%.osm: Ski/%.gpx
	gpsbabel -i gpx -f $< -o osm,tag="type:route;route:ski;piste_type:nordic" -F $@

mystyle/mytype.typ: mystyle/mytype.txt
	cd mystyle && $(JAVA) -jar $(MKGMAP) mytype.txt

gmapsupp.img: $(ATV_OBJ) $(Hike_OBJ) $(Bike_OBJ) $(Ski_OBJ) mystyle/mytype.typ
	$(JAVA) -jar $(MKGMAP) --remove-ovm-work-files --style-file=mystyle --route --index --family-id=1123 --mapname=11230001 --output-dir=output --series-name="SSM Trails Map" --description="SSM Trails Map" --country-name=canada --country-abbr=ca --draw-priority=30 --transparent --gmapsupp $(ATV_OBJ) $(Hike_OBJ) $(Bike_OBJ) $(Ski_OBJ) mystyle/mytype.typ

nsis:
	$(JAVA) -jar $(MKGMAP) --remove-ovm-work-files --style-file=mystyle --route --index --family-id=1123 --mapname=11230001 --output-dir=output --series-name="SSM Trails Map" --description="SSM Trails Map" --country-name=canada --country-abbr=ca --draw-priority=30 --transparent --nsis $(ATV_OBJ) $(Hike_OBJ) $(Bike_OBJ) $(Ski_OBJ) mystyle/mytype.typ
